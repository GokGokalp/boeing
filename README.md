# Boeing Case

## Task

- Implement a small web application which should show 5 biggest folders from a specified path.

### Requirements

- The solution should consist of a Web page and Web API.
- The page should show a list of 5 folders ordered by size from biggest to smaller.
- It should be implemented in .Net core.
- The UI should be implemented as SPA. Preferably in latest Angular.
- Web API endpoint should be implemented using async/await pattern.
- Include some tests.

### Solutions

- "src" folder includes both backend and UI applications.
- "tests" folder includes some tests that are written with NUnit, AutoFixture, FluentAssertions, Moq.
- I developed the solution on MacOS using VS Code.

#### UI

- I used Angular 8 and material components.
- I tried to use Observable.
- I created a service to call API.
- I declared some models such as enums, folder and request model.
- I used material table to list biggest documents.
- I used material sorting & pagination components for some client-side operations.
- I created local functions (because according to the case, I just need one component) to show or hide messages or loading bar.

#### Backend

- At the first look, my solutions maybe seem like over-engineering a bit. But, actually not. I think so :), Also I didn't get any requirement about the API structure. Because of, I developed like that.
- Backend application consists of two-layer.
- I implemented minimal clean architecture (ports&adapters).
- I used MediatR for query side in order to provide completely loosely-coupling between layers.
- With MediatR's adapter, "GetBiggestFoldersHandler" every service/handler has single responsibility completely.
- The controller of the API doesn't know to any service interface contract. It just uses the "IMediator" interface.
- I tried to keep clean the "Core" project. The core project is the heart of the application. It includes domain models, Dtos and services.
- I'm getting "path, size and sort" parameters as request parameter.
- I used the internal response wrapper approach with "BaseResponseDto" class. It provided me to handle exceptions easily.
- In use-cases handlers, I didn't isolate the validation part. In clean architecture, generally, each use--cases includes own validations.
- I implemented async/await approach.
- I implemented Swagger UI.
- I used CorrelationId package to add correlation id in messages.
- If the request success and the path is correct, I return 200.
- If the path does not exist, I return 404.
- If the request null or path is emtpy, I return 400.
- If the path contains a folder which is not accessible, I return 403.
- If the path contains a not allowed character, I return 400.
- If the path is not a directory, I return 400.
