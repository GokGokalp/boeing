﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BoeingCase.Core.Dtos;
using BoeingCase.Core.Dtos.Requests;
using BoeingCase.Core.Dtos.Responses;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace BoeingCase.API.Controllers
{
    [Route("api/biggest-paths")]
    [ApiController]
    public class BiggestFoldersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public BiggestFoldersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK)]
        [SwaggerResponse((int)HttpStatusCode.BadRequest)]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        [SwaggerResponse((int)HttpStatusCode.Forbidden)]
        [SwaggerResponse((int)HttpStatusCode.InternalServerError)]
        public async Task<ActionResult<FolderDto>> Get([FromQuery]GetBiggestFoldersRequest request)
        {
            BaseResponseDto<List<FolderDto>> getBiggestFoldersResponse = await _mediator.Send(request);

            if (!getBiggestFoldersResponse.HasError)
            {
                return Ok(getBiggestFoldersResponse.Data);
            }
            else
            {
                var message = getBiggestFoldersResponse.Messages.FirstOrDefault();

                return StatusCode((int)message.HttpStatusCode, message.Message);
            }
        }
    }
}