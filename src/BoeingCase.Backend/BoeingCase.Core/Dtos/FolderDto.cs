namespace BoeingCase.Core.Dtos
{
    public class FolderDto
    {
        public string Name { get; set; }
        public long Size { get; set; }
    }
}