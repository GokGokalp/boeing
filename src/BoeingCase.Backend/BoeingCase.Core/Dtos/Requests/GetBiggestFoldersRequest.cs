using System.Collections.Generic;
using BoeingCase.Core.Common;
using BoeingCase.Core.Dtos.Responses;
using MediatR;

namespace BoeingCase.Core.Dtos.Requests
{
    public class GetBiggestFoldersRequest : IRequest<BaseResponseDto<List<FolderDto>>>
    {
        public string Path { get; set; }
        public int Count { get; set; }
        public Constants.Sort Sort { get; set; }
    }
}