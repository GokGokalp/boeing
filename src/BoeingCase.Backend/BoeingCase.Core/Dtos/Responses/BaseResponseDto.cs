using System.Collections.Generic;
using System.Linq;

namespace BoeingCase.Core.Dtos.Responses
{
    public class BaseResponseDto<TData>
    {
        public BaseResponseDto()
        {
            Messages = new List<ResponseMessageDto>();
        }

        public bool HasError => Messages.Any(x => x.IsError);
        public List<ResponseMessageDto> Messages { get; set; }
        public int Total { get; set; }
        public TData Data { get; set; }
    }
}