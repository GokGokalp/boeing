using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using BoeingCase.Core.Common;
using BoeingCase.Core.Dtos;
using BoeingCase.Core.Dtos.Requests;
using BoeingCase.Core.Dtos.Responses;
using CorrelationId;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace BoeingCase.Core.Services.UseCases
{
    public class GetBiggestFoldersHandler : IRequestHandler<GetBiggestFoldersRequest, BaseResponseDto<List<FolderDto>>>
    {
        private readonly ILogger<GetBiggestFoldersHandler> _logger;
        private readonly ICorrelationContextAccessor _correlationContextAccessor;
        private readonly IConfiguration _configuration;

        public GetBiggestFoldersHandler(ILogger<GetBiggestFoldersHandler> logger, ICorrelationContextAccessor correlationContextAccessor, IConfiguration configuration)
        {
            _logger = logger;
            _correlationContextAccessor = correlationContextAccessor;
            _configuration = configuration;
        }

        public Task<BaseResponseDto<List<FolderDto>>> Handle(GetBiggestFoldersRequest request, CancellationToken cancellationToken)
        {
            var biggestFoldersResponse = new BaseResponseDto<List<FolderDto>>();

            try
            {
                BaseResponseDto<bool> pathValidationResponse = ValidateParameters(request);

                if (pathValidationResponse.HasError)
                {
                    biggestFoldersResponse.Messages.AddRange(pathValidationResponse.Messages);

                    _logger.LogError(string.Join(",", pathValidationResponse.Messages));

                    return Task.FromResult(biggestFoldersResponse);
                }

                IEnumerable<DirectoryInfo> folders = new DirectoryInfo(request.Path).EnumerateDirectories();

                biggestFoldersResponse.Data = new List<FolderDto>();

                foreach (DirectoryInfo directory in folders)
                {
                    long size = directory.EnumerateFiles("*.*", SearchOption.AllDirectories).AsParallel()
                        .Sum(x => x.Length);

                    biggestFoldersResponse.Data.Add(new FolderDto
                    {
                        Name = directory.Name,
                        Size = size
                    });
                }

                if (biggestFoldersResponse.Data.Count > 0)
                {
                    IOrderedEnumerable<FolderDto> orderedFolders = request.Sort == Constants.Sort.Desc ? biggestFoldersResponse.Data.OrderByDescending(x => x.Size) : biggestFoldersResponse.Data.OrderBy(x => x.Size);

                    biggestFoldersResponse.Data = orderedFolders.Take(request.Count).ToList();
                }

            }
            catch (Exception ex)
            {
                string message = string.Empty;
                HttpStatusCode statusCode;

                if (ex.InnerException?.GetType() == typeof(UnauthorizedAccessException))
                {
                    message = $"You don't have a permission to access some folders. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}";

                    statusCode = HttpStatusCode.Forbidden;
                }
                else
                {
                    message = $"An error occurred while getting biggest folders. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}";

                    statusCode = HttpStatusCode.InternalServerError;
                }

                _logger.LogError(ex, message);

                biggestFoldersResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    Message = message,
                    HttpStatusCode = statusCode
                });
            }

            return Task.FromResult(biggestFoldersResponse);
        }

        private BaseResponseDto<bool> ValidateParameters(GetBiggestFoldersRequest request)
        {
            var pathValidationResponse = new BaseResponseDto<bool>();

            if (request == null)
            {
                pathValidationResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.BadRequest,
                    Message = $"Parameters can not be null. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}"
                });

                return pathValidationResponse;
            }

            if (string.IsNullOrEmpty(request.Path))
            {
                pathValidationResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.BadRequest,
                    Message = $"The path parameter can not be null. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}"
                });

                return pathValidationResponse;
            }

            if (request.Count <= 0)
            {
                pathValidationResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.BadRequest,
                    Message = $"The count parameter can not be less or equal than zero. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}"
                });

                return pathValidationResponse;
            }

            IEnumerable<string> notAllowedCharacters = _configuration.GetValue<string>("NotAllowedCharacters")?.Split(',').Select(x => x);

            if (notAllowedCharacters != null)
            {
                foreach (string _char in notAllowedCharacters)
                {
                    if (request.Path.Contains(_char))
                    {
                        pathValidationResponse.Messages.Add(new ResponseMessageDto
                        {
                            IsError = true,
                            HttpStatusCode = HttpStatusCode.BadRequest,
                            Message = $"The path contains not allowed characters. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}"
                        });

                        return pathValidationResponse;
                    }
                }
            }

            if (File.Exists(request.Path))
            {
                pathValidationResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.BadRequest,
                    Message = $"The specified path is not a directory. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}"
                });

                return pathValidationResponse;
            }

            if (!Directory.Exists(request.Path))
            {
                pathValidationResponse.Messages.Add(new ResponseMessageDto
                {
                    IsError = true,
                    HttpStatusCode = HttpStatusCode.NotFound,
                    Message = $"The specified path does not found. Error Code: {_correlationContextAccessor.CorrelationContext.CorrelationId}"
                });

                return pathValidationResponse;
            }

            pathValidationResponse.Data = true;

            return pathValidationResponse;
        }
    }
}