import { Component, OnInit, ViewChild } from '@angular/core';
import { Folder } from 'src/app/models/folder';
import { BiggestFoldersService } from 'src/app/services/biggest-folders.service';
import { GetBiggestFoldersRequestModel } from 'src/app/models/get-biggest-folders-request-model';
import { SortType, StatusType } from 'src/app/models/app-enums';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  loaderIsVisible: boolean;
  statusMessageDisplay: boolean;
  statusMessage: string;
  foldersDataTable: MatTableDataSource<Folder>;
  displayedColumns: string[];
  getBiggestFoldersRequest: GetBiggestFoldersRequestModel;
  keys = Object.keys;
  sortTypes = SortType;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private biggestFoldersService: BiggestFoldersService) { 
    this.loaderIsVisible = false;
    this.displayedColumns = ['name', 'size'];

    this.getBiggestFoldersRequest = <GetBiggestFoldersRequestModel>
    {
      path: "",
      count: 5,
      sort: SortType.Desc
    };

    this.showDisplayMessage("No folders found.");
  }

  ngOnInit() {
  }

  getBiggestFolders(){
    this.showLoader();

    this.biggestFoldersService.getBiggestFolders(this.getBiggestFoldersRequest).subscribe(resp => {
      if (resp.status === StatusType.OK) {
        if(resp.body.length > 0){
          this.foldersDataTable = new MatTableDataSource<Folder>(resp.body);

          this.foldersDataTable.paginator = this.paginator;
          this.foldersDataTable.sort = this.sort;

          this.hideDisplayMessage();
        }else{
          this.showDisplayMessage("No folders found.");
        }
      }else{
        this.showDisplayMessage("No folders found.");
      }
    }, error => {
      if(typeof this.foldersDataTable !== 'undefined'){
        this.foldersDataTable.data = [];
      }

      this.hideLoader();

      this.showDisplayMessage(error.error);

    }, () => { this.hideLoader();})
  }

  showDisplayMessage(message: string){
    this.statusMessage = message;
    this.statusMessageDisplay = true; 
  }

  hideDisplayMessage(){
    this.statusMessage = "";
    this.statusMessageDisplay = false;
  }

  showLoader(){
    this.loaderIsVisible = true;
  }

  hideLoader(){
    this.loaderIsVisible = false;
  }
}