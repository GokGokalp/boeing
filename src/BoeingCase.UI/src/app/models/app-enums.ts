export enum SortType {
    Asc = "Asc",
    Desc = "Desc"
}

export enum StatusType {
    OK = 200
}