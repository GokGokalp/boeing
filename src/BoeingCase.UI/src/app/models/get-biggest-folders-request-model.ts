import { SortType } from './app-enums';

export class GetBiggestFoldersRequestModel {
    path: string;
    count: number;
    sort: SortType;
}
