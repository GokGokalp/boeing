import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Folder } from '../models/folder';
import { GetBiggestFoldersRequestModel } from '../models/get-biggest-folders-request-model';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class BiggestFoldersService {

  constructor(private http : HttpClient) { }

  getBiggestFolders(request: GetBiggestFoldersRequestModel) : Observable<HttpResponse<Folder[]>> {
    return this.http.get<Folder[]>(API_URL + '/biggest-paths?path=' + request.path + '&count=' + request.count + '&sort=' + request.sort, { observe: 'response' });
  }
}