using BoeingCase.Core.Services.UseCases;
using Microsoft.Extensions.Logging;
using CorrelationId;
using Moq;
using NUnit.Framework;
using AutoFixture;
using Microsoft.Extensions.Configuration;
using BoeingCase.Core.Dtos.Requests;
using BoeingCase.Core.Common;
using BoeingCase.Core.Dtos.Responses;
using BoeingCase.Core.Dtos;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using FluentAssertions;
using System.Net;
using System;
using System.IO;
using Microsoft.Extensions.Logging.Internal;

namespace Tests
{
    [TestFixture]
    public class GetBiggestFoldersHandlerUnitTests
    {
        private GetBiggestFoldersHandler _getBiggestFoldersHandler;

        private IFixture _fixture;
        private Mock<ILogger<GetBiggestFoldersHandler>> _mockLogger;
        private IConfiguration _configuration;
        private Mock<ICorrelationContextAccessor> _mockCorrelationContextAccessor;
        private string _correlationId;

        [SetUp]
        public void Setup()
        {
            _fixture = new Fixture();
            _mockLogger = new Mock<ILogger<GetBiggestFoldersHandler>>();

            _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.Development.json")
                .Build();

            _correlationId = _fixture.Create<string>();

            _mockCorrelationContextAccessor = new Mock<ICorrelationContextAccessor>();

            var correlationContextFactory = new CorrelationContextFactory();
            _mockCorrelationContextAccessor.SetupGet(_ => _.CorrelationContext)
                .Returns(correlationContextFactory.Create(_correlationId, _fixture.Create<string>()));

            _getBiggestFoldersHandler = new GetBiggestFoldersHandler(_mockLogger.Object, _mockCorrelationContextAccessor.Object, _configuration);
        }

        [Test]
        public async Task Handle_WhenAPathPassedAsAParameterThatIsContainSmallSubFoldersAtLeast5_ShouldReturn5BiggestFoldersAsOrderedBySizeDesc()
        {
            //Arrange
            var getBiggestFoldersRequest = new GetBiggestFoldersRequest
            {
                Path = "/users/gokhangokalp/projects",
                Count = 5,
                Sort = Constants.Sort.Desc
            };
            var cancellationToken = _fixture.Create<CancellationToken>();

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeFalse();
            biggestFoldersResponse.Data.Count.Should().Be(getBiggestFoldersRequest.Count);
            biggestFoldersResponse.Data.Should().BeInDescendingOrder(x => x.Size);
        }

        [Test]
        public async Task Handle_WhenAPathPassedAsAParameterThatIsContainSmallSubFoldersAtLeast5_ShouldReturn5BiggestFoldersAsOrderedBySizeAsc()
        {
            //Arrange
            var getBiggestFoldersRequest = new GetBiggestFoldersRequest
            {
                Path = "/users/gokhangokalp/projects",
                Count = 5,
                Sort = Constants.Sort.Asc
            };
            var cancellationToken = _fixture.Create<CancellationToken>();

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeFalse();
            biggestFoldersResponse.Data.Count.Should().Be(getBiggestFoldersRequest.Count);
            biggestFoldersResponse.Data.Should().BeInAscendingOrder(x => x.Size);
        }

        [Test]
        public async Task Handle_WhenTheRequestObjectPassedAsNull_ShouldReturnErrorWithBadRequestStatusCode()
        {
            //Arrange
            GetBiggestFoldersRequest getBiggestFoldersRequest = null;
            var cancellationToken = _fixture.Create<CancellationToken>();

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeTrue();
            biggestFoldersResponse.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.BadRequest);
            biggestFoldersResponse.Messages.Should().Contain(x => x.Message == $"Parameters can not be null. Error Code: {_correlationId}");
            _mockLogger.Verify(l => l.Log(LogLevel.Error, 0, It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()));
        }

        [Test]
        public async Task Handle_WhenThePathParameterPassedAsNull_ShouldReturnErrorWithBadRequestStatusCode()
        {
            //Arrange
            var getBiggestFoldersRequest = new GetBiggestFoldersRequest
            {
                Path = null,
                Count = 5,
                Sort = Constants.Sort.Asc
            };
            var cancellationToken = _fixture.Create<CancellationToken>();

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeTrue();
            biggestFoldersResponse.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.BadRequest);
            biggestFoldersResponse.Messages.Should().Contain(x => x.Message == $"The path parameter can not be null. Error Code: {_correlationId}");
            _mockLogger.Verify(l => l.Log(LogLevel.Error, 0, It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()));
        }

        [Test]
        public async Task Handle_WhenTheCountParameterLessOrEqualThanZero_ShouldReturnErrorWithBadRequestStatusCode()
        {
            //Arrange
            var getBiggestFoldersRequest = new GetBiggestFoldersRequest
            {
                Path = "/users:",
                Count = 0,
                Sort = Constants.Sort.Asc
            };
            var cancellationToken = _fixture.Create<CancellationToken>();

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeTrue();
            biggestFoldersResponse.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.BadRequest);
            biggestFoldersResponse.Messages.Should().Contain(x => x.Message == $"The count parameter can not be less or equal than zero. Error Code: {_correlationId}");
            _mockLogger.Verify(l => l.Log(LogLevel.Error, 0, It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()));
        }

        [Test]
        public async Task Handle_WhenAPathPassedAsAParameterThatIsContainRestrictedSubFolders_ShouldReturnErrorWithForbiddenStatusCode()
        {
            //Arrange
            var getBiggestFoldersRequest = new GetBiggestFoldersRequest
            {
                Path = "/usr",
                Count = 5,
                Sort = Constants.Sort.Asc
            };
            var cancellationToken = _fixture.Create<CancellationToken>();

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeTrue();
            biggestFoldersResponse.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.Forbidden);
            biggestFoldersResponse.Messages.Should().Contain(x => x.Message == $"You don't have a permission to access some folders. Error Code: {_correlationId}");
            _mockLogger.Verify(l => l.Log(LogLevel.Error, 0, It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()));
        }

        [Test]
        public async Task Handle_WhenAPathPassedAsAParameterThatIsAFile_ShouldReturnErrorWithBadRequestStatusCode()
        {
            //Arrange
            var getBiggestFoldersRequest = new GetBiggestFoldersRequest
            {
                Path = "/users/gokhangokalp/tmp.json",
                Count = 5,
                Sort = Constants.Sort.Asc
            };
            var cancellationToken = _fixture.Create<CancellationToken>();

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeTrue();
            biggestFoldersResponse.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.BadRequest);
            biggestFoldersResponse.Messages.Should().Contain(x => x.Message == $"The specified path is not a directory. Error Code: {_correlationId}");
            _mockLogger.Verify(l => l.Log(LogLevel.Error, 0, It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()));
        }

        [Test]
        public async Task Handle_WhenAPathPassedAsAParameterThatIsNotExist_ShouldReturnErrorWithNotFoundStatusCode()
        {
            //Arrange
            var getBiggestFoldersRequest = new GetBiggestFoldersRequest
            {
                Path = "/users/gokhangokalp/notexist",
                Count = 5,
                Sort = Constants.Sort.Asc
            };
            var cancellationToken = _fixture.Create<CancellationToken>();

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeTrue();
            biggestFoldersResponse.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.NotFound);
            biggestFoldersResponse.Messages.Should().Contain(x => x.Message == $"The specified path does not found. Error Code: {_correlationId}");
            _mockLogger.Verify(l => l.Log(LogLevel.Error, 0, It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()));
        }

        [Test]
        public async Task Handle_WhenAnInternalExceptionOccured_ShouldReturnErrorWithInternalServerErrorStatusCode()
        {
            //Arrange
            var getBiggestFoldersRequest = new GetBiggestFoldersRequest
            {
                Path = "/users",
                Count = 5,
                Sort = Constants.Sort.Asc
            };
            var cancellationToken = _fixture.Create<CancellationToken>();

           _configuration = null;
           _getBiggestFoldersHandler = new GetBiggestFoldersHandler(_mockLogger.Object, _mockCorrelationContextAccessor.Object, _configuration);

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeTrue();
            biggestFoldersResponse.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.InternalServerError);
            biggestFoldersResponse.Messages.Should().Contain(x => x.Message == $"An error occurred while getting biggest folders. Error Code: {_correlationId}");
            _mockLogger.Verify(l => l.Log(LogLevel.Error, 0, It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()));
        }

        [Test]
        public async Task Handle_WhenThePathParameterContainsNotAllowedCharacters_ShouldReturnErrorWithBadRequestStatusCode()
        {
            //Arrange
            var getBiggestFoldersRequest = new GetBiggestFoldersRequest
            {
                Path = "/users:",
                Count = 5,
                Sort = Constants.Sort.Asc
            };
            var cancellationToken = _fixture.Create<CancellationToken>();

            //Act
            BaseResponseDto<List<FolderDto>> biggestFoldersResponse = await _getBiggestFoldersHandler.Handle(getBiggestFoldersRequest, cancellationToken);

            //Assert
            biggestFoldersResponse.HasError.Should().BeTrue();
            biggestFoldersResponse.Messages.Should().Contain(x => x.HttpStatusCode == HttpStatusCode.BadRequest);
            biggestFoldersResponse.Messages.Should().Contain(x => x.Message == $"The path contains not allowed characters. Error Code: {_correlationId}");
            _mockLogger.Verify(l => l.Log(LogLevel.Error, 0, It.IsAny<FormattedLogValues>(), It.IsAny<Exception>(), It.IsAny<Func<object, Exception, string>>()));
        }
    }
}